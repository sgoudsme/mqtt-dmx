FROM python:3

ADD mqtt_injection.py /home/
ADD dmx/ /home/dmx

RUN pip install paho-mqtt==1.5.0 pyyaml==5.1 pyserial==3.4 simplejson==3.16.0

CMD [ "python", "-u", "/home/mqtt_injection.py" ]
