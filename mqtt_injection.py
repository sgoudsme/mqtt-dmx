import time
import json
import os
import yaml
from dmx.DMX_Serial import DMX_Serial
from dmx.MqttHandler import MqttHandler

# Read config file
dir_path = os.path.dirname(os.path.realpath(__file__))
config = None
with open(dir_path + '/config.yaml') as file:
    config = yaml.full_load(file)


# init empty array with dmx data
dmx_target = [0] * 512
dmx_transition = [0] * 512
dmx_current = [0] * 512
dmx_int = [0] * 512
last_time = 0

# Callback function when mqtt message is received
def mqtt_mess_recv(topic, payload):
    channel = int(topic.split('/')[1]) -1
    print(channel)
    message = json.loads(payload)
    print((message))

    if "ON" in message["state"]:
        dmx_target[channel] = 255

    if "OFF" in message["state"]:
        dmx_target[channel] = 0

    if "brightness" in message:
        dmx_target[channel] = message["brightness"]

    if "transition" in message:
        dmx_transition[channel] = (dmx_target[channel] - dmx_int[channel] ) / (float(message["transition"]) * 1000)
    else:
        dmx_transition[channel] = (dmx_target[channel] - dmx_int[channel] )
    print (dmx_transition[channel])

    print("something received on topic")

# Init mqtt connection
subscr_topic = [config['topic_name'] + "/+/command"]
mqtt = MqttHandler(config['mqtt_broker'], subscr_topic, config['mqtt_username'], config['mqtt_password'], mqtt_mess_recv)

while not mqtt.connected:
    pass

# Init dmx and start sending data
sender = DMX_Serial()
sender.start()

print("all good")
# Block forever
transitioning = False
while(True):
    millis = int(round(time.time() * 1000))
    if not transitioning:
        time.sleep(0.1)
    if millis > last_time:
        last_time = millis
        transitioning = False
        for i in range(512):
            if dmx_int[i] != dmx_target[i]:
                dmx_current[i] += dmx_transition[i]
                transitioning = True
            if dmx_current[i] > 255:
                dmx_current[i] = 255
            if dmx_current[i] < 0:
                dmx_current[i] = 0
            dmx_int[i] = int(dmx_current[i])

        data = bytes(dmx_int)
        sender.set_data(data)
