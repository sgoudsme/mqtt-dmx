# DMX485
## Requirements
This script requires pyserial and pyyaml
```
pip install --user pyserial
pip install pyyaml
```

## Hardware Compatibility
This should work with any FTDI RS485 dongle or any of the similar knock-offs.

## Fill in yaml file
- Mqtt broker ip
- Mqtt username and password (optional)
- topic name where to get dmx values

## MQTT usage
The mqtt message layout is compatible with the home assistant light integration (see: https://www.home-assistant.io/integrations/light.mqtt)

It is necessary to set:
- brightness: true
- schema: json

The topic needs to be of format topicName/ccc/command where:
- topicName is the name given in the config file
- ccc is the dmx channel number
- command is a word that needs to be there literally

Example of the topic name: "D1234/25/command"

The payload is alwyas a json string with following keys:
- state (ON/OFF) (required)
- brightness (0-255) (optional)
- transition (time in sec) (optional)

The time can be precise to 0.001 sec.

Example of the payload:
- {"state": "ON", "transition": 10.0, "brightness": 255}
- {"state": "ON", "transition": 10.0}
- {"state": "ON", "brightness": 255}
- {"state": "ON"}

## running the script:
```
python3 mqtt_injection.py
```
